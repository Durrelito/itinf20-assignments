from random import randint, choice

bold_start = "\033[1m"
bold_end = "\033[0m"

firstnames = ["Samuel", "Connor", "Janna", "Gilbert", "Viktor", "Therese", "André", "Tom", "Daniel", "Morgan",
              "Raman", "Alexander", "Karl", "Liam", "Malin", "Martin", "Sid", "Philip", "Piotr", "Joakim",
              "Grigorios", "Johhny", "Marcus", "Sebastian", "Ludvig", "Filip", "Ramy", "Andreas", "Henry",
              "William", "Christoffer", "Martin", "Malek", "Christoffer"]

lastnames = ["Avery", "Cisternas", "Dahlberg", "Dikmen", "Espinoza", "From", "Gaulitz", "Gilmartin", "Gomez",
             "Hammerborg", "Hersan", "Hjerpe", "Ingves", "Jobark", "Joelsson", "Jönsson", "Knutsson",
             "Kojhassarly", "Kubacki", "Kuurne", "Kyriakoudis", "Larsson", "Lindell", "Lindén", "Lindholm",
             "Lundgren", "Matti", "Odenfelt", "Palmgren", "Rinne", "Ritter", "Sääf", "Saliba", "Skilving"]


class Person :
    def __init__(self) :
        self.firstname = choice(firstnames)
        self.lastname = choice(lastnames)
        self.birthdate = randint(1980, 1999)


class Player(Person) :
    def __init__(self) :
        super().__init__()
        self.speed = randint(4, 10)
        self.agility = randint(4, 10)
        self.strength = randint(4, 10)

    def __str__(self) :
        return f" {bold_start}Player:{bold_end} {self.firstname} {self.lastname}, Birthday: {self.birthdate}, " \
               f"Speed: {self.speed}, Agility: {self.agility}, Strength: {self.strength}."

    def take_steroids(self) :
        self.speed = self.speed + randint(1, 10)
        self.agility = self.agility + 10
        self.strength = self.strength + randint(1, 10)

    def play_game(self, players) :
        total_spd = 0
        total_agi = 0
        total_str = 0
        for stats in players :
            total_spd += stats.speed
            total_agi += stats.agility
            total_str += stats.strength

        total = (total_spd - total_agi) + total_str

        return total


class Coach(Person) :
    def __init__(self) :
        super().__init__()
        self.level = randint(2, 5)

    def __str__(self) :
        return f" {bold_start}Coach:{bold_end} {self.firstname} {self.lastname}, Birthdate: {self.birthdate}, Voice" \
               f"-level: {self.level}. "

    def megaphone(self) :
        self.level = self.level + 7


class Team :
    def __init__(self, coach, players) :
        self.coach = coach
        self.players = players

    def team_comp(self) :
        print(self.coach)
        for players in self.players :
            print(players)


def coachboost(coaches, players) :
    if coaches.level < 10 :
        for i in players :
            i.speed = i.speed + 3
            i.agility = i.agility + 3
            i.strength = i.strength + 3

    elif coaches.level > 10 :
        for i in players :
            i.speed = i.speed - 3
            i.agility = i.agility - 3
            i.strength = i.strength - 3


coach1 = Coach()
coach2 = Coach()

players1 = [Player(),
            Player(),
            Player(),
            Player(),
            Player(),
            Player(),
            Player(),
            Player(),
            Player()]

players2 = [Player(),
            Player(),
            Player(),
            Player(),
            Player(),
            Player(),
            Player(),
            Player(),
            Player()]

team1 = Team(coach1, players1)
team2 = Team(coach2, players2)

team_total = Player().play_game(players1)
team_total2 = Player().play_game(players2)

if __name__ == "__main__" :
    players1[1].take_steroids()
    players2[3].take_steroids()
    coach1.megaphone()
    coach2.megaphone()

    coachboost(coach1, players1)
    coachboost(coach2, players2)

    print(f"                                {bold_start}Team One:{bold_end}")
    print("-----------------------------------------------------------------------------")
    team1.team_comp()
    print("-----------------------------------------------------------------------------")
    print(f"                                {bold_start}Team Two: {bold_end}")
    print("-----------------------------------------------------------------------------")
    team2.team_comp()
    print("-----------------------------------------------------------------------------")
    print("")
    play_game = input(f"      {bold_start}Do you want to pit these teams against each other? (Yes/No){bold_end}")

    if play_game == "Yes" or play_game == "yes" :

        if team_total2 > team_total :
            print("")
            print(f"        Congratulations Team One, you won! With a score of {team_total2} - {team_total}.")

        elif team_total > team_total2 :
            print("")
            print(f"        Congratulations Team One, you won! With a score of {team_total} - {team_total2}.")

    else :
        print("")
        print("                             That's too bad.")
