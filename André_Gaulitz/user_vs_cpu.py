# local library imports
from wordagent import WordAgent
from utils import importer, conditions, text_output


def user_vs_cpu(file) :                             # user vs cpu gamemode
    text_output(f"\n\t\t\tGame instructions:\n\n"
                f"\t\t- The CPU will randomly choose a word.\n"
                f"\t\t- You will try to guess it.\n"
                f"\t\t- After every guess the CPU will return:\n"
                f"\t\t    1. Letters that is in the correct position.\n"
                f"\t\t    2. Letters that is in the word.\n"
                f"\t\t    3. If none of the letters matched.\n"
                f"\t\t- Enter 'quit' to return to main menu.")

    wordlist = importer(file)
    cpu = WordAgent(wordlist=wordlist)
    cpu.random_word()
    user = WordAgent()

    turn = 1
    while True :                                    # main while loop for vs_cpu
        print("\n\t\t\tEnter your guess (5 letters).\n\n")
        user.user_guess("(wordgame): ")
        conditions(f"\n\t\tCongratulations! You guessed the correct word: {cpu.word} in {turn} turns.",
                   f"\n\t\tThe correct word was {cpu.word}, you endured {turn} turns.", user.guess, cpu.word, file)
        correct_position, correct_letter = cpu.comparison(user)
        comparison_output(correct_position, correct_letter, user.guess)
        turn += 1


def comparison_output(correct_position, correct_letter, guess) :  # outputs the correct words joined with ","
    cp_join = ",".join(correct_position)
    cl_join = ",".join(correct_letter)

    if correct_position and correct_letter :
        text_output(f"\n\t\t{guess}: letter(s) [{cp_join}] is in "
                    f"the correct position and [{cl_join}] is in the word.")

    elif correct_position :
        text_output(f"\n\t\t{guess}: letter(s) [{cp_join}] is in the right position.")

    elif correct_letter :
        text_output(f"\n\t\t{guess}: letter(s) [{cl_join}] is in the word.")

    else :
        text_output(f"\n\t\t{guess}: no letters matched.")
