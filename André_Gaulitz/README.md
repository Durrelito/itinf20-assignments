# Wordgame

Av André Gaulitz


1. Starta programmet från exec.py.
2. Välj spelläge.

### Spelläge ett:
```
- Datorn väljer ett ord utifrån sin ordlista och 
  det är spelarens uppgift att gissa på ordet.
- Efter varje gissning kommer programmet återge:
- 1. Bokstäver som var på rätt plats.
  2. Bokstäver som finns med i ordet. 
  3. Om det inte fanns något matchning alls.
- Med feedbacken kan spelaren gissa igen.
- Skriv "quit" för att återgå till startmenyn.
```

### Spelläge två:
```
- Spelaren kommer på ett ord med fem bokstäver och skriver in det.
- Datorn gissar på ett ord utifrån sin ordlista.
- Efter varje gissning skall spelaren återge feedback per bokstav:
- 1. 1 = bokstaven är i rätt position. 
  2. 2 = bokstaven finns i ordet.
  3. 0 = bokstaven finns inte i ordet. 
- Utifrån feedbacken tar datorn bort ord ur sin ordlista och gissar igen.
- Skriv "quit" innan spelet startar för att återgå till startmenyn.
```

### Spelläge tre:
```
- Dator 1 väljer ett ord från ordlistan.
- Dator 2 gissar på ett ord.
- Likt spelläge två återger Dator 1 feedback beroende på:
- 1. Bokstav i rätt position.
  2. Bokstav finns i ordet.
  3. Bokstav finns inte i ordet.
- Denna loop fortsätter tills Dator 2 har gissat rätt.
```

### Spelläge fyra:
```
- Spelaren har 7 rundor på sig att gissa datorns ord.
- När 3 rundor återstår får spelaren hjälp med första bokstaven i datorns ord.
- Skriv "quit" för att återgå till startmenyn.
```

3. Efter varje omgång tas man tillbaka till huvudmenyn.
