# local library imports
from wordagent import WordAgent
from utils import conditions, importer, text_output


def cpu_vs_cpu(file):                    # cpu vs cpu gamemode
    text_output(f"\n\t\t\t\tGame instructions:\n\n"
          f"\t\t- CPU1 will choose a word and CPU2 will try to guess it.\n"
          f"\t\t- CPU1 will then give feedback after every guess.\n"
          f"\t\t- The game will go on until CPU2 guess the right word.\n"
          f"\t\t- Sit back and relax.")

    wordlist = importer(file)
    cpu1 = WordAgent(wordlist=wordlist)
    cpu1.random_word()
    cpu2 = WordAgent(wordlist=wordlist)

    turn = 1
    while True :                                # main while loop
        cpu2.random_guess()
        conditions(f"\n\t\tCPU2 successfully guessed CPU1's word: {cpu1.word}, in {turn} turns.",
                   "", cpu2.guess, cpu1.word, file)
        cpu_feedback = feedback(cpu2.guess, cpu1.word)
        text_output(f"\n\t\tCPU2's {turn} guess is {cpu2.guess}\n"
                    f"\t\tCPU1's feedback is {cpu_feedback}")
        cpu2.eliminate_words(cpu_feedback)
        turn += 1


def feedback(guess, word):                # emulates feedback like in cpu vs user
    cpu_feedback = []

    for i, letter in enumerate(guess) :
        if letter == word[i]:
            cpu_feedback.append(1)

        elif letter in word :
            cpu_feedback.append(2)

        elif letter not in word :
            cpu_feedback.append(0)

    return cpu_feedback
