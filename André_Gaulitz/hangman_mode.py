# standard library imports
from time import sleep

# local library imports
from hangman import Hangman
from utils import importer, clear, delayed_printer, conditions, main_menu, text_output
from ascii_art import ascii_hangman, you_died


def hangman(file) :                         # hangman gamemode
    text_output("\n\t\t\t Game instructions:\n\n"
          "\t\t- You have 7 turns to guess the CPU's word.\n"
          "\t\t- When 3 turns remain, you'll get the first letter in the word.\n"
          "\t\t- Enter 'quit' to return to main menu.")

    wordlist = importer(file)
    cpu = Hangman(wordlist=wordlist)
    cpu.random_word()
    user = Hangman()

    turns = 7
    index = 0
    while turns > 0 :                       # main while loop
        turns -= 1
        ascii_print(index, cpu.letter_list)
        user.user_guess("(wordgame): ")
        conditions(f"\n\t\tYou correctly guessed: {cpu.word}, with {turns} turns left.",
                   "\n\t\tReturning to main menu.", user.guess, cpu.word, file)
        cpu.hangman_comparison(user)
        cpu.first_letter(turns)
        index += 1

    fail_state(file)


def ascii_print(index, letter_list) :             # prints hangman ascii and letter list
    clear()
    print(f"{ascii_hangman[index]}\n"
          f"\t\t{letter_list}\n")


def fail_state(file) :                              # prints you died ascii art and returns to main menu
    clear()
    delayed_printer(you_died, 0.15)
    sleep(2)
    main_menu("\n\t\tReturning to main menu.", file)
