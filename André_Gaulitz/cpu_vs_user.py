# third party imports
from tabulate import tabulate

# local library imports
from wordagent import WordAgent
from utils import *


def cpu_vs_user(file) :                                     # cpu vs user gamemode
    text_output("\n\t\t\t\tGame instructions:\n\n"
          "\t\t- Enter a word with five letters and the computer will return a guess.\n"
          "\t\t- The computer needs feedback for every letter in the word:\n"
          "\t\t   1 = Letter is in the right position.\n"
          "\t\t   2 = Letter is in the word but in the wrong position.\n"
          "\t\t   0 = Letter is not in the word.\n"
          "\t\t- The computer will then remove words accordingly and present the words that are left.\n"
          "\t\t- Enter 'quit' before starting, to return to main menu.")

    print("\n\t\tEnter your word (5 letters).\n\n")
    user_word = string_check("(wordgame): ")
    user = WordAgent(word=user_word)
    wordlist = importer(file)
    cpu = WordAgent(wordlist=wordlist)

    turn = 1
    while True :                            # main while loop for vs_user
        cpu.random_guess()
        conditions(f"\n\t\tThe CPU successfully guessed: {cpu.guess} in {turn} turns.",
                   "\n\t\tReturning to main menu.", user.word, cpu.guess, file)
        cpu.eliminate_words(feedback(cpu.guess, user.word, turn))
        words_left(cpu.wordlist)
        cheat_checker(user.word, cpu, file)
        turn += 1


def feedback(guess, word, turn) :               # loops through every letter of the CPU's guess and
    user_feedback = []                          # appends the feedback to a list and returns it.

    for letter in guess :
        clear()
        print(f"\n\t\t\tMy {turn} guess is {guess}\n\n"
              f"\t\tLetter: {letter}\t\tYour word: {word}\n\n")

        feedback_input = int_input("(wordgame): ")
        user_feedback.append(feedback_input)
    return user_feedback


def words_left(wordlist) :              # if the list isn't empty it presents the words left in CPU's wordlist
    if wordlist :
        loading("\n\t\t\tLoading wordlist", 3)
        text_output(tabulate(list(list_chunks(wordlist, 10))))


def list_chunks(wordlist, size) :               # takes a large list and turns it into smaller
    for i in range(0, len(wordlist), size) :    # lists inside a list for tabulate output.
        yield wordlist[i :i + size]             # https://www.studytonight.com/post/python-splitting-a-list-into-evenly-sized-chunks


def cheat_checker(word, cpu, file) :         # if the objects wordlist is empty it initializes the cheat_check function
    if not cpu.wordlist :
        loading("\n\t\tWordlist empty, cheat check in progress", 3)
        list_check(word, cpu, file)
        main_menu("\n\t\tReturning to main menu.", file)


def list_check(word, cpu, file) :               # checks if chosen word exists in removed words
    if word in cpu.removed :
        text_output(f"\n\t\tI found {word} in my wordlist, incorrect feedback given.")

    else :
        check_cleared(word, file)


def check_cleared(word, file) :                 # asks if you want to add the word to wordlist
    clear()
    print(f"\n\t\t\tNo cheat detected, add {word} to wordlist?\n"
          f"\t\t\t1. Yes.\n"
          f"\t\t\t2. No.\n\n")
    user_input = int_input("(wordgame): ")

    if user_input == 1 :
        append_word(word, file)

    else :
        pass


def append_word(word, file) :                   # adds word to given file
    with open(file, "a+") as txt :
        txt.write(f"\n{word}")
        txt.close()

        text_output(f"\n\t\t{word} added.")
