# local library import
from cpu_vs_user import cpu_vs_user
from user_vs_cpu import user_vs_cpu
from cpu_vs_cpu import cpu_vs_cpu
from hangman_mode import hangman
from utils import int_input, clear, delayed_printer
from ascii_art import menu_logo


def menu(file) :
    clear()

    menu_text = ["\n\t\t\t\t\t\t    Main menu:",
                 "\t\t\t\t\t\t1. User versus CPU",
                 "\t\t\t\t\t\t2. CPU versus User",
                 "\t\t\t\t\t\t3. CPU versus CPU",
                 "\t\t\t\t\t\t4. Hangman",
                 "\t\t\t\t\t\t0. Quit game\n\n"]

    delayed_printer(menu_logo, 0.15)
    delayed_printer(menu_text, 0.15)

    while True :
        user_input = int_input("(wordgame): ")

        if user_input == 1 :
            user_vs_cpu(file)

        elif user_input == 2 :
            cpu_vs_user(file)

        elif user_input == 3 :
            cpu_vs_cpu(file)

        elif user_input == 4 :
            hangman(file)

        elif user_input == 0 :
            quit(0)






