# local library imports
from wordagent import WordAgent


class Hangman(WordAgent) :
    def __init__(self, guess=None, word=None, wordlist=None) :
        super().__init__(guess, word, wordlist)
        self.letter_list = ["_", "_", "_", "_", "_"]

    def hangman_comparison(self, user):                 # adds letters to list if in correct position.
        self.guess = user.guess

        for i, letter in enumerate(self.guess) :
            if letter == self.word[i] :
                self.letter_list[i] = letter

    def first_letter(self, turn) :                      # sets the first letter of the word when there's 3 turns left.
        if turn < 4 :
            self.letter_list[0] = self.word[0]


