# standard library imports
import os
from time import sleep

# local library imports
import menu


def importer(path) :                                     # opens text files and appends each row,
    words = []                                           # stripped of \n and returns a sorted list.

    with open(path) as txt :
        for row in txt :
            words.append(row.strip())

        return sorted(words)


def string_check(text):                    # loops until word meets criteria and handles unicode exceptions
    while True :
        try :
            word = input(text).lower()

            if len(word) == 5 or word == "quit" :
                return word

        except UnicodeDecodeError :
            pass


def int_input(question) :           # handles int valueerror exceptions.
    while True :
        try :
            return int(input(question))

        except ValueError :
            pass


def clear() :
    if os.name == 'posix' :                     # clears terminal for Mac / Linux users
        _ = os.system('clear')

    else :                                      # clears terminal for Windows users
        _ = os.system('cls')


def enter_continue() :                          # waits for any input
    input("\n(wordgame): Enter to continue.")


def loading(text, times) :                  # simulates loading
    clear()
    print(text, end='')
    for i in range(times) :
        sleep(1)
        print('.', end='')


def delayed_printer(text_list, seconds) :      # simulates loading a game, prints lists/text with delay
    for entry in text_list :
        print(entry)
        sleep(seconds)


def main_menu(text, file) :                 # simulates loading and returns to main menu
    loading(text, 3)
    menu.menu(file)


def conditions(text1, text2, word1, word2, file):               # win and quit condition
    if word1 == word2 :
        main_menu(text1, file)

    elif word1 == "quit" :
        main_menu(text2, file)


def text_output(text) :                                    # useful print function
    clear()
    print(text)
    enter_continue()
    clear()
