# standard library imports
from random import choice

# local library imports
from utils import string_check


class WordAgent :
    def __init__(self, guess=None, word=None, wordlist=None) :
        self.guess = guess
        self.word = word
        self.wordlist = wordlist
        self.removed = []

    def random_guess(self):
        self.guess = choice(self.wordlist)

    def random_word(self):
        self.word = choice(self.wordlist)

    def user_guess(self, text):
        self.guess = string_check(text)

    def comparison(self, user) :                         # checks every letter and adds them to
        self.guess = user.guess                          # a string if index position match or they're in the word.
        correct_position = ""
        correct_letter = ""

        for i, letter in enumerate(self.guess) :        # from stackoverflow, https://stackoverflow.com/questions/66654799/compare-two-strings-count-letters-in-the-right-position-then-count-letters-con/66655244#66655244
            if letter == self.word[i] :
                correct_position += letter
            elif letter in self.word :
                correct_letter += letter

        return correct_position, correct_letter

    def eliminate_words(self, feedback) :                        # used to remove words according to given feedback,
        for i, number in enumerate(feedback) :                   # when self.guess is a wordlist and adds the removed
            for word in self.wordlist :                          # words to a new list for cheat check later
                if number == 1 and self.guess[i] != word[i] :
                    self.remove_and_save(word)

                elif number == 2 and self.guess[i] not in word :
                    self.remove_and_save(word)

                elif number == 0 and self.guess[i] in word :
                    self.remove_and_save(word)

    def remove_and_save(self, word) :                           # removes and appends words
        self.wordlist.remove(word)
        self.removed.append(word)
