class Person :

    def __init__(self, firstname, lastname, birthdate, address) :
        self.firstname = firstname
        self.lastname = lastname
        self.birthdate = birthdate
        self.address = address

    def sql_insert(self, connection) :
        cursor = connection.cursor()                # checks if object already exists in database.
        cursor.execute(f"""SELECT Firstname, Lastname,                     
                        Birthdate, Address from Person WHERE 
                        Firstname =? AND
                        Lastname =? AND
                        Birthdate =? AND
                        Address =?""", (self.firstname, self.lastname, self.birthdate, self.address))

        if cursor.fetchone() is None :              # if object doesn't exist, it adds it to database.
            connection.execute(f"""INSERT INTO Person 
                                (Firstname,
                                Lastname,
                                Birthdate,
                                Address) 
                                VALUES 
                                (?, ?, ?, ?)""", (self.firstname, self.lastname, self.birthdate, self.address))
