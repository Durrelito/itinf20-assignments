# local application imports
import sql
from utils import clear, int_input, waiting
from car import Car


def table_maker(connection) :  # menu for creating default tables or defining a new one.

    while True :
        clear()
        print("1. Use default table.\n"
              "2. Define a new table.\n"
              "3. Proceed.")

        choice = int_input("Input: ")

        if choice == 1 :
            clear()
            sql.create_default(connection)
            waiting()

        elif choice == 2 :
            clear()
            sql.define_table(connection)
            waiting()

        elif choice == 3 :
            break


def menu(connection, person_list, cars_list) :  # main menu

    person_entry = ""
    cars_entry = ""

    while True :
        clear()
        print("1. Load data from file.\n"
              f"2. List all entries in person table. \t{person_entry}\n"
              f"3. List all entries in cars table.\t{cars_entry}\n"
              "4. Add entry to cars table.\n"
              "5. Query person database.\n"
              "6. List car ownership.\n"
              "7. Delete an entry in person table.\n"
              "8. Update address.\n"
              "9. Create table.\n"
              "10. Exit program.")

        choice = int_input("Input:")

        if choice == 1 :  # inserts person and car objects into database.
            sql.insert_data(connection, person_list, cars_list)
            person_entry = "*New entries*"
            cars_entry = "*New entries*"

        elif choice == 2 :  # prints print_person function.
            sql.print_persons(connection)
            person_entry = ""

        elif choice == 3 :  # prints print_cars function.
            sql.print_cars(connection)
            cars_entry = ""

        elif choice == 4 :
            clear()
            Car().add_car(connection)
            cars_entry = "*Entry updated*"

        elif choice == 5 :  # uses query_database function.
            query_database(connection)

        elif choice == 6 :  # prints print_join function.
            sql.print_join(connection)

        elif choice == 7 :  # deletes chosen id's entry.
            sql.delete_entry(connection)

            person_entry = "*Entry deleted*"

        elif choice == 8 :  # updates chosen id's address.
            sql.update_address(connection)

            person_entry = "*Entry updated*"

        elif choice == 9 :            # returns you to table creator menu.
            table_maker(connection)

        elif choice == 10 :  # closes database connection and quits program with exit code 0.
            connection.close()
            quit()


def query_database(connection) :  # menu for querying database and retrieving rows

    while True :
        clear()
        print("1. Query database with firstname.\n"
              "2. Query database with lastname.\n"
              "3. Query database with birthdate.\n"
              "4. Query database with address.\n"
              "5. Return to main menu.")

        choice = int_input("Input: ")

        if choice == 1 :  # looks up user input against firstnames in database.
            sql.query_firstname(connection)

        elif choice == 2 :  # looks up user input against lastnames in database.
            sql.query_lastname(connection)

        elif choice == 3 :  # looks up user input against birthdate in database.
            sql.query_birthdate(connection)

        elif choice == 4 :  # looks up user input against address in database.
            sql.query_address(connection)

        else :
            break
