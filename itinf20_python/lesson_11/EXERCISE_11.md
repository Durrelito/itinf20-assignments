# Group Exercise Lesson 11

## Instructions

This is probably your first slightly larger program, so you will tackle it as a group exercise. Each team consists of 3-4 members. You can write your program fully object-oriented or not, this is up to your team to decide. The only requirement is that you somewhere use a class `Person`.

### `Exercise 1` Load data from a file

1. Create a file in either csv or json format. It should contain data for at least 5 Persons, you can create it manually or with a script.
2. A person has a firstname, lastname, birthdate and address.
3. Read the file and insert it into to your SQLite database.

### `Exercise 2` List all persons

1. Create a query that get all person rows (select *)
2. Print all rows

### `Exercise 3` Menu

1. Create a menu with options:
   1. Load data from file
   2. List all persons (select *)
      1. EXTRA query the database with firstname
      2. EXTRA query the database with lastname
      3. EXTRA query the database with birthdate
      4. EXTRA query the database with address
   3. Delete a person
   4. EXTRA update a persons address

### EXTRA EXTRA Add another table

Create another table, i.e Vehicles, Friends or Transactions.

1. Add it to your menu
2. Make it possible to add data to the second table
3. Query the database with a join
