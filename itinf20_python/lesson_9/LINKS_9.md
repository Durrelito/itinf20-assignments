# Links

## Python

- Python docs [Data Model](https://docs.python.org/3/reference/datamodel.html#data-model)
- Python tutorial [Classes](https://docs.python.org/3/tutorial/classes.html#classes)

## Git

- Git [cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
