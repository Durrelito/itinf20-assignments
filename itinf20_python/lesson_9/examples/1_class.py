
# Minimal class

class A:
    pass

# Instantiating a Class
# Create a object


a = A()
print(a)  # <__main__.A object at 0x101adcd90>
print(type(a))  # <class '__main__.A'>

# Add a attribute to a instance

a.color = 'red'
print(a.color)

# Try to use the attribute in another instance
a_2 = A()
# print(a_2.color)

# A class attribute
A.color = 'blue'
a_3 = A()
a_4 = A()

print(a_3.color)
print(a_4.color)

# Class attributes will be the same for all instances

A.color = 'black'

print(a_3.color)
print(a_4.color)
