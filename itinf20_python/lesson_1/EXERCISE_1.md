# Exercise 1

## Instructions

The main goal of this exercise is to learn how to use GitLab and Git. We will verify that git, visual studio code and python works properly on your environment. You will also learn how to use markdown, a lightweight markup language commonly used in i.e GitHub, GitLab, but also for docs.

### `Exercise` python

Make sure to install python 3.9 [download](https://www.python.org/downloads/)

```bash
python --version
```

If you prefer powershell

```PowerShell
py -0
```

#### Hand in instructions python

- [ ]  Provide a screenshot showing your python version

### `Exercise` git

Git [download](https://git-scm.com/downloads)

```bash
git --version
```

#### Hand in instructions Git

- [ ]  Provide a screenshot showing your Git version

### `Exercise` markdown

1. Create a branch with name `YOURUSERNAME_lesson_1`
2. Create a markdown file `lesson_1/YOURUSERNAME/HELLO.md` with random text.
3. Install a preview extension for markdown in your Visual Studio Code IDE. Check `LINKS.md` for help.
4. Commit and push your markdown file.

#### Hand in instructions markdown

- [ ]  Provide a screenshot of the Visual Studio preview of your markdown
- [ ]  Provide a screenshot of the GitLab showing your README.md

### General instructions

Submit screenshot[s] and file[s] by a `Merge Request` in repository [itinf20_python](https://gitlab.com/robert-alfwar/itinf20_python)

- [ ]  Create a branch with name `YOURUSERNAME_lesson_1`
- [ ]  Create a additional folder in `lesson_1` that is named `YOURUSERNAME`
- [ ]  Place screenshot[s] and/or file[s] in the created folder `lesson_1/YOURUSERNAME`
- [ ]  Assign the merge request to the teacher.
