# Links

## Shell

- Shell alternative for linux & mac with autocomplete, highlighting [ohmyz](https://ohmyz.sh/)

## Python

- python 3.9 [download](https://www.python.org/downloads/)
- Mac Brew about [Python](https://docs.brew.sh/Homebrew-and-Python)

## Git

- Install git [download](https://git-scm.com/downloads)
- Book about git [Git Pro](https://git-scm.com/book/en/v2)
- GitLab git [cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

## GitLab

- Git repositories located at [GitLab](https://gitlab.com/)

### Markdown

- Cheat sheet for GitLab [cheat sheet](https://gitlab.com/francoisjacquet/rosariosis/-/wikis/Markdown-Cheatsheet)
- GitLab offical documentation for markdown [markdown specification](https://docs.gitlab.com/ee/user/markdown.html)
- Code block in GitLab [List of supported languages](https://github.com/rouge-ruby/rouge/wiki/List-of-supported-languages-and-lexers)
- [Visual Studio Code extension for markdown](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
