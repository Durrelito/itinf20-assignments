import sqlite3
from sqlite3 import OperationalError
from tabulate import tabulate

dataset = (("Nils", "Smith"), ("Adam", "Williams"), ("Joe", "Clinton"))

try:
    with sqlite3.connect(':memory:') as conn:
        conn.execute(
            """
            CREATE TABLE customers (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                firstname TEXT,
                lastname TEXT
            )
            """)

        conn.execute(
            """
            CREATE TABLE customers (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                firstname TEXT,
                lastname TEXT
            )
            """)

# Catch the specific sqlite3 error
except OperationalError as e:
    print(e)
finally:
    conn.close()
