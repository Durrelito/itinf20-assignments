import sqlite3
from tabulate import tabulate

dataset = (("Nils", "Smith"), ("Adam", "Williams"), ("Joe", "Clinton"))

try:
    with sqlite3.connect(':memory:') as conn:
        conn.execute(
            """
            CREATE TABLE customers (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                firstname TEXT,
                lastname TEXT
            )
            """)

        conn.executemany(
            """
            INSERT INTO customers (
                firstname,
                lastname
            ) VALUES(?, ?)
            """, dataset)

        customers = conn.execute(
            """
            SELECT *
            FROM customers
            """
        )

        description = tuple(map(lambda x: x[0], customers.description))
        print(tabulate(customers.fetchall(), description))

except Exception as e:
    print(e)
finally:
    conn.close()
