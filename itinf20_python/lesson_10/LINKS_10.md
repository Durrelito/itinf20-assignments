# Links

## Python

- Python simple framework for CLI [cmd](https://docs.python.org/3/library/cmd.html)
- Python inheritance vs composition short text [geek for geeks](https://www.geeksforgeeks.org/inheritance-and-composition-in-python/)

### Extra Python

- Python inheritance vs composition long text [real python](https://realpython.com/inheritance-composition-python)

## Git

- Git [cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
