# Exercise 3

## Instructions

The goal of this exercise is to learn about Built-in Python Data Types, expressions, comparisons and strings

### `Exercise 1` create a branch

If you want to save your work, you will need to create a branch.

1. Check with git status, it should output

    ```text
    On branch master
    Your branch is up to date with 'origin/master'.

    nothing to commit, working tree clean
    ```

2. git branch `YOURUSERNAME_lesson_3`
3. git switch `YOURUSERNAME_lesson_3`
4. Check with git status, it should output

    ```text
    On branch YOURUSERNAME_lesson_3
    nothing to commit, working tree clean
    ```

### `Exercise 2` Data Types

see studentportalen `Datatyper_extra_övning.pdf`

### `Exercise 3` Sorting

In LINKS_3.md there is a link Python guide how-to do [sorting](https://docs.python.org/3/howto/sorting.html). Create a list containing 10 car brand. i.e cars = ["volvo", ...]

1. Sort the list with `sorted(cars)`
2. Sort the list with `cars.sort()`
3. reverse the sort of both, read more about reversing in python docs [ascending-and-descending](https://docs.python.org/3/howto/sorting.html#ascending-and-descending)
4. *Extra* exercise, create a list of 10 tuples containing (brand, model), i.e ("volvo", "xc90"). Sort first on brand, then on model.
