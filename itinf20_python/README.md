# ITINF20_PYTHON

## General

You can view markdown files in GitLab i.e this `README.md` will be visible on the page [itinf20_python](https://gitlab.com/robert-alfwar/itinf20_python).

You can preview markdown files in vscode with an extension i.e [markdown all in one](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one). To open the preview mode, right click on the file and select `Open preview`.

```bash
# To get the latest lesson & updates in master
git pull

# If you are in a branch you can rebase
git fetch
git rebase origin/master
```

## Lesson folders

Each lesson has its own folder that often contain a `LINK.md` and `EXERCISE.md`.

### Exercise

Exercises should generally `NOT` be handed in, except the first lesson and the group project. You are recommended to complete all exercises, since they are preparing you for the final assignment and future lessons.

### Link

The `LINK.md` file contains what you need to solve the exercises and you can also find extra material to learn more. If you get stuck on an exercise, remember to check links for that lesson.

### Improvements

If you have any improvements, guides or just better wording, feel free to create a `merge request` with the improvement to this repository.
